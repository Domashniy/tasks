/* 
 * File:   SphereApproximation.h
 * Author: vasya
 *
 * Created on January 24, 2016, 4:27 PM
 */

#ifndef SPHEREAPPROXIMATION_H
#define	SPHEREAPPROXIMATION_H

#include <vector>
#include <Eigen/Dense>

struct SphereParams {
    float x0,y0,z0;
    float r;
};
class SphereApproximation {
public:
    SphereApproximation();
    virtual ~SphereApproximation();
    static std::vector<Eigen::Vector3f> createSpherePointsCloud(SphereParams params);
    static SphereParams solveSphere(std::vector<Eigen::Vector3f> cloud);
    
private:
    static Eigen::Vector3f calcCOM(std::vector<Eigen::Vector3f> cloud);
    static Eigen::MatrixXd solveIter(std::vector<Eigen::Vector3f> cloud, float r0);
    static void solveHT(std::vector<Eigen::Vector3f> cloud);
};

#endif	/* SPHEREAPPROXIMATION_H */

