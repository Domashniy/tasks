var dir_617ecf4707934de2318090466f038e83 =
[
    [ "Composite", "dir_3cde2ca7af9740c5b0d23b8cc04dfa69.html", "dir_3cde2ca7af9740c5b0d23b8cc04dfa69" ],
    [ "CatmullClarkT.hh", "a00594.html", [
      [ "CatmullClarkT", "a00104.html", "a00104" ]
    ] ],
    [ "CompositeLoopT.hh", "a00595.html", [
      [ "CompositeLoopT", "a00114.html", "a00114" ],
      [ "EVCoeff", "a00145.html", "a00145" ],
      [ "compute_weight", "a00120.html", "a00120" ]
    ] ],
    [ "CompositeSqrt3T.hh", "a00596.html", [
      [ "CompositeSqrt3T", "a00115.html", "a00115" ],
      [ "FVCoeff", "a00160.html", "a00160" ],
      [ "compute_weight", "a00121.html", "a00121" ]
    ] ],
    [ "LongestEdgeT.hh", "a00597.html", [
      [ "CompareLengthFunction", "a00113.html", "a00113" ],
      [ "LongestEdgeT", "a00200.html", "a00200" ]
    ] ],
    [ "LoopT.hh", "a00598.html", "a00598" ],
    [ "ModifiedButterFlyT.hh", "a00599.html", [
      [ "ModifiedButterflyT", "a00223.html", "a00223" ]
    ] ],
    [ "Sqrt3InterpolatingSubdividerLabsikGreinerT.hh", "a00600.html", "a00600" ],
    [ "Sqrt3T.hh", "a00601.html", "a00601" ],
    [ "SubdividerT.hh", "a00602.html", "a00602" ]
];