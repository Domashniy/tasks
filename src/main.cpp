
#include <iostream>
#include <fstream>

#include "SurfaceHandler.h"
#include "SphereApproximation.h"

using namespace std;
using namespace Eigen;

string filePath;

bool parseArgs(int argc, char *argv[]);
vector<Vector3f> openMesh2Eigen(vector<OpenMesh::Vec3f> pts);
void saveParams(vector<SphereParams> params, std::string fileName);
void saveCloud(vector<Vector3f> cloud, std::string fileName);


int main(int argc, char *argv[]) {

    if (!parseArgs(argc, argv))
        return 1;


    SurfaceHandler s(filePath);
    if(!s.isOpened()) {
        return 1;
    }
        
    vector<Vector3f> allPts;
    vector<Vector3f> initialSurfPts = openMesh2Eigen(s.getRawPoints());
    allPts.insert(allPts.end(), initialSurfPts.begin(), initialSurfPts.end());

    vector<SphereParams> speresParams;
    
    for (auto pc : s.getClusters()) {
        SphereParams params = SphereApproximation::solveSphere(openMesh2Eigen(pc));
        speresParams.push_back(params);
        vector<Vector3f> spherePts = SphereApproximation::createSpherePointsCloud(params);
        allPts.insert(allPts.end(), spherePts.begin(), spherePts.end());
    }
    saveParams(speresParams, "res.txt");
    saveCloud(allPts, "cloud.xyz");
    

    return 0;

}


vector<Vector3f> openMesh2Eigen(vector<OpenMesh::Vec3f> pts) {
    vector<Vector3f> newPts;
    for (auto p : pts) {
        newPts.push_back(Vector3f(p[0], p[1], p[2]));
    }
    return newPts;
}

void saveParams(vector<SphereParams> params, std::string fileName) {
    ofstream f(fileName);
    f << "  R  " << "  x0  " << "  y0  " << "  z0  "<<  endl;
    for (SphereParams p : params) {
        f << p.r << " " << p.x0 << " " << p.y0 << " " << p.z0 <<endl;
    }
    f << endl;
    f.close();
}

void saveCloud(vector<Vector3f> cloud, std::string fileName) {
    ofstream f(fileName);
    for (Vector3f p : cloud) {
        f << p[0] << " " << p[1] << " " << p[2] << endl;
    }
    f << endl;
    f.close();
}


bool parseArgs(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "too few arguments" << endl;
        cout << "Run as ./SphereRecognition your_file_with_mesh.ply" << endl;
        return false;
    }
    filePath = string(argv[1]);
    return true;

}






