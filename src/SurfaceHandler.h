/* 
 * File:   SurfaceHandler.h
 * Author: vasya
 *
 * Created on January 23, 2016, 2:00 PM
 */

#ifndef SURFACEHANDLER_H
#define	SURFACEHANDLER_H

#include <set>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/IO/reader/PLYReader.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <OpenMesh/Core/System/config.h>
#include <OpenMesh/Core/Mesh/Status.hh>


struct MyTraits : public OpenMesh::DefaultTraits {
    VertexAttributes(OpenMesh::Attributes::Status);
    FaceAttributes(OpenMesh::Attributes::Status);
    EdgeAttributes(OpenMesh::Attributes::Status);
};


typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

struct Circle {

    Circle(std::vector<MyMesh::HalfedgeHandle> edgesHandles, bool isClockwise,
            std::set<int> attendedIdxs) :
    edgesHandles(edgesHandles),
    clockwise(isClockwise),
    attendedIdxs(attendedIdxs) {
    }

    std::vector<MyMesh::HalfedgeHandle> edgesHandles;
    std::set<int> attendedIdxs;

    bool clockwise;
};

class SurfaceHandler {
public:
    SurfaceHandler(std::string fileName);
    bool isOpened();
    virtual ~SurfaceHandler();
    std::vector<OpenMesh::Vec3f> getRawPoints();
    std::vector<std::vector<OpenMesh::Vec3f>> getClusters();

private:
    int deleteBorder(Circle border, bool useNormalCheck);
    std::vector<Circle> findBorders();
    Circle getBorderWith(MyMesh::HalfedgeHandle hehInit);
    void calcAverNorm();
    void cutBadBorder(int iters);
    void deleteBackgroundPolys();
    void collectRawPointsCloud();
    void clusterize();
    void findNeighbor(MyMesh::FaceFaceCWIter fIt,
            std::vector<MyMesh::FaceHandle>& faces, std::set<int>& attendedFaces);
    void collectInternalPoints(Circle c); 

    MyMesh mesh;
    OpenMesh::Vec3f averNorm;
    std::vector<OpenMesh::Vec3f> rawPointsCloud;
    std::vector<Circle> currentBorders;
    std::vector<std::vector<OpenMesh::Vec3f>> currentClusters;
    bool opened;
};

#endif	/* SURFACEHANDLER_H */

