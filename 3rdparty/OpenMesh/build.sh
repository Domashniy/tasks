#!/bin/bash 
mkdir build
mkdir install
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install .. -DBUILD_APPS=false
make -j9
make install

