/* 
 * File:   SphereApproximation.cpp
 * Author: vasya
 * 
 * Created on January 24, 2016, 4:27 PM
 */
#include <iostream>
#include "SphereApproximation.h"
using Eigen::Vector3f;
using Eigen::MatrixXd;
using std::vector;
using std::cout;
using std::endl;

SphereApproximation::SphereApproximation() {
}

SphereApproximation::~SphereApproximation() {
}
SphereParams SphereApproximation::solveSphere(vector<Vector3f> cloud) {
    SphereParams params;
    const int iters = 2;
    float r0 = calcCOM(cloud).norm();
    MatrixXd res;
    for (int i = 0; i < iters; i++) {
        res = solveIter(cloud, r0);
        Vector3f c(res(0, 0), res(1, 0), res(2, 0));
        r0 = c.norm();
    }
    params.r  = sqrt(res(3, 0));
    params.x0 = res(0, 0);
    params.y0 = res(1, 0);
    params.z0 = res(2, 0);

    cout << "R " << params.r << " x0 " << params.x0 
                             << " y0 " << params.y0 
                             << " z0 " << params.z0  << endl;
    
    return params;
}
MatrixXd SphereApproximation::solveIter(vector<Vector3f> cloud, float r0) {
    int rows = cloud.size();

    MatrixXd A(rows, 4);
    MatrixXd b(rows, 1);
    MatrixXd x(4, 1);
    int i = 0;
    for (Vector3f p : cloud) {
        A.row(i) << 2 * p[0], 2 * p[1], 2 * p[2], 1.0;
        b.row(i) << r0 * r0 + p.norm() * p.norm();
        i++;
    }

    x = A.colPivHouseholderQr().solve(b);
    return x;
}
Vector3f SphereApproximation::calcCOM(vector<Vector3f> cloud) {

    Vector3f res(0, 0, 0);

    for (Vector3f p : cloud) {
        res += p;
    }
    res /= (float) cloud.size();
    return res;
}

vector<Vector3f> SphereApproximation::createSpherePointsCloud(SphereParams params) {
    const float s = 0.2;
    const float phiMax = 2*M_PI; 
    const float thetaMax = M_PI;
    vector<Vector3f> pts;

    for (float phi = 0.f; phi < phiMax; phi += s) {
        for (float theta = 0.f; theta < thetaMax; theta += s) {

            float x = params.x0 + params.r * sin(theta) * cos(phi);
            float y = params.y0 + params.r * sin(theta) * sin(phi);
            float z = params.z0 + params.r * cos(theta);
            Vector3f p(x, y, z);
            pts.push_back(p);
        }
    }
    return pts;
}

//interesting methode, but does not work well
void SphereApproximation::solveHT(vector<Vector3f> cloud) {
    int rMin = 10;
    int rMax = 70;
    int cxs = 400, cys = 200, czs = 60, rs = 70;

    vector<int> a(cxs * cys * czs * rs);
    memset(a.data(), 0, 4 * cxs * cys * czs * rs);

    for (Vector3f p : cloud) {
#pragma omp parallel for
        for (int r = rMin; r < rMax; r++) {
            for (float theta = 0; theta < 2 * M_PI; theta += 0.2) {
                for (float phi = 0; phi < M_PI; phi += 0.2) {
                    int cx = round(p[0] - r * cos(theta) * sin(phi));
                    int cy = round(p[1] - r * sin(theta) * sin(phi));
                    int cz = round(p[2] - r * cos(phi));
                    if (cx < 0 || cy < 0 || cz < 0)
                        continue;
                    a[r * cxs * cys * czs + cx * cys * czs + czs * cy + cz]++;
                }
            }
        }
    }

    int max = 0;
    int R, cx0, cy0, cz0;
    for (int r = 0; r < rs; r++) {
        for (int cx = 0; cx < cxs; cx++) {
            for (int cy = 0; cy < cys; cy++) {
                for (int cz = 0; cz < czs; cz++) {
                    int index = r * cxs * cys * czs + cx * cys * czs + czs * cy + cz;
                    int tmp = a[index];
                    if (tmp > max) {
                        R = r;
                        cx0 = cx;
                        cy0 = cy;
                        cz0 = cz;
                        max = tmp;
                    }

                }
            }
        }

    }

    cout << "R " << R << endl;
    cout << "cx0 " << cx0 << endl;
    cout << "cy0 " << cy0 << endl;
    cout << "cz0 " << cz0 << endl;

}