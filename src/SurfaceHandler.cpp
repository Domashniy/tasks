/* 
 * File:   SurfaceHandler.cpp
 * Author: vasya
 * 
 * Created on January 23, 2016, 2:00 PM
 */

#include "SurfaceHandler.h"
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::set;
using std::vector;

SurfaceHandler::SurfaceHandler(std::string fileName) {

    OpenMesh::IO::PLYReader();
    if (!OpenMesh::IO::read_mesh(mesh, fileName)) {
        cout << "Error opening ply: " << fileName << endl;
        opened = false;
    }
    else {
        opened = true;
        collectRawPointsCloud();
        calcAverNorm();
        cutBadBorder(4);
        deleteBackgroundPolys();

        clusterize();
    }
}

SurfaceHandler::~SurfaceHandler() {
}

void SurfaceHandler::clusterize() {

    const int minBorderLength = 20;
    int i = 0;
    for (auto cb : currentBorders) {
        int borderLength = cb.edgesHandles.size();

        if (borderLength > minBorderLength) {
            collectInternalPoints(cb);
            i++;
        }
    }
}

void SurfaceHandler::findNeighbor(MyMesh::FaceFaceCWIter fIt, vector<MyMesh::FaceHandle>& faces, set<int>& attendedFaces) {
    int added = 0;
    vector<MyMesh::FaceHandle> curFaces;
    for (int i = 0; i < 3; i++) {
        if (fIt.is_valid()) {
            if (attendedFaces.count(fIt->idx()) == 0) {
                attendedFaces.insert(fIt->idx());
                curFaces.push_back(*fIt);
                added++;
            }
        }
        ++fIt;
    }
    for (auto f : curFaces) {
        faces.push_back(f);
    }

    for (auto f : curFaces) {
        MyMesh::FaceFaceCWIter fItNew = mesh.ff_cwiter(f);
        findNeighbor(fItNew, faces, attendedFaces);
    }


}

void SurfaceHandler::collectInternalPoints(Circle c) {
    bool stop = false;
    set<int> attendedFaces;
    set<int> addedPoints;
    vector<MyMesh::FaceHandle> faces;
    MyMesh::HalfedgeHandle heh = c.edgesHandles[0];
    vector<OpenMesh::Vec3f> curCloud;

    MyMesh::FaceHandle fh = mesh.face_handle(mesh.opposite_halfedge_handle(heh));
    MyMesh::FaceFaceCWIter fIt = mesh.ff_cwiter(fh);

    findNeighbor(fIt, faces, attendedFaces);
    for (auto f : faces) {
        MyMesh::ConstFaceVertexIter fvIt(mesh.fv_iter(f));
        for (int i = 0; i < 3; i++) {
            if (addedPoints.count(fvIt->idx()) == 0) {
                addedPoints.insert(fvIt->idx());
                const MyMesh::Point & p(mesh.point(*fvIt));
                curCloud.push_back(OpenMesh::Vec3f(p[0], p[1], p[2]));
            }
            ++fvIt;
        }
    }

    currentClusters.push_back(curCloud);
}

void SurfaceHandler::cutBadBorder(int iters) {
    for (int i = 0; i < iters; i++) {
        vector<Circle> borders = findBorders();

        for (auto c : borders) {
            deleteBorder(c, false);
            break;
        }
        mesh.garbage_collection();
    }
}

void SurfaceHandler::deleteBackgroundPolys() {
    for (int i = 0; i < 1000; i++) {
        currentBorders = findBorders();

        int deleted = 0;
        for (auto c : currentBorders) {
            deleted += deleteBorder(c, true);
        }

        mesh.garbage_collection();

        if (deleted == 0) {
            break;
        }
    }
}

int SurfaceHandler::deleteBorder(Circle border, bool useNormalCheck) {

    int deleted = 0;
    int step = 0;
    for (auto heh : border.edgesHandles) {
        step++;
        if (!heh.is_valid()) {

            cout << "error" << endl;
            continue;
        }
        MyMesh::FaceHandle fh = mesh.face_handle(mesh.opposite_halfedge_handle(heh));
        if (!fh.is_valid()) {
            fh = mesh.face_handle((heh));
            if (!fh.is_valid()) {
                continue;
            }
        }

        if (!useNormalCheck) {
            mesh.delete_edge(mesh.edge_handle(heh), true);
            deleted++;
            continue;
        }
        OpenMesh::Vec3f n = mesh.calc_face_normal(fh);
        float c = fabs(n | averNorm) / (n.norm() * averNorm.norm());
        const float minAngle = 0.83;
        if (c > minAngle) {
            mesh.delete_edge(mesh.edge_handle(heh), true);
            deleted++;
        }
    }
    return deleted;
}

Circle SurfaceHandler::getBorderWith(MyMesh::HalfedgeHandle hehInit) {
    vector<MyMesh::HalfedgeHandle> attendedEdges;
    set<int> attendedIdxs;
    MyMesh::HalfedgeHandle heh = hehInit;
    OpenMesh::Vec3f prevVec, vec;
    float dir = 0;

    do {
        attendedIdxs.insert(heh.idx());
        attendedEdges.push_back(heh);
        vec = mesh.calc_edge_vector(heh);
        vec[2] = 0;
        float r = (vec % prevVec)[2];
        dir += r;
        prevVec = vec;
        heh = mesh.next_halfedge_handle(heh);
    }
    while (heh != hehInit);
    Circle res(attendedEdges, dir < 0 ? true : false, attendedIdxs);

    return res;
}

vector<Circle> SurfaceHandler::findBorders() {

    MyMesh::HalfedgeIter hIt, hEnd(mesh.halfedges_end());
    set<int> attendedEdges;
    vector<Circle> attendedEdgesGrouped;

    int steps = 0;
    for (hIt = mesh.halfedges_begin(); hIt != hEnd; ++hIt) {
        if (!mesh.is_boundary(*hIt) || attendedEdges.count(hIt->idx()) > 0) {
            continue;
        }

        MyMesh::HalfedgeHandle hehInit =
            mesh.halfedge_handle(mesh.to_vertex_handle(*hIt));

        Circle circle = getBorderWith(hehInit);
        int size = circle.edgesHandles.size();
        bool dir = circle.clockwise;
        attendedEdges.insert(circle.attendedIdxs.begin(), circle.attendedIdxs.end());
        attendedEdgesGrouped.push_back(circle);

        steps++;
    }

    return attendedEdgesGrouped;
}

void SurfaceHandler::collectRawPointsCloud() {
    MyMesh::VertexIter vIt, vEnd(mesh.vertices_end());
    for (vIt = mesh.vertices_begin(); vIt != vEnd; ++vIt) {
        const MyMesh::Point& p = mesh.point(*vIt);
        rawPointsCloud.push_back(OpenMesh::Vec3f(p[0], p[1], p[2]));
    }
}

vector<OpenMesh::Vec3f> SurfaceHandler::getRawPoints() {
    return rawPointsCloud;
}

void SurfaceHandler::calcAverNorm() {
    MyMesh::FaceIter fIt, fEnd(mesh.faces_end());
    int i = 0;

    for (fIt = mesh.faces_begin(); fIt != fEnd; ++fIt) {

        OpenMesh::Vec3f v = mesh.calc_face_normal(*fIt);

        averNorm += v;
        i++;
    }
    averNorm /= i + 1;
    averNorm /= averNorm.norm();

}

vector<vector<OpenMesh::Vec3f> > SurfaceHandler::getClusters() {
    return currentClusters;
}

bool SurfaceHandler::isOpened() {
    return opened;
}
