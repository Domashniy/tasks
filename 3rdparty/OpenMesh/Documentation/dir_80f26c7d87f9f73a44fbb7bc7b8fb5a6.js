var dir_80f26c7d87f9f73a44fbb7bc7b8fb5a6 =
[
    [ "BaseDecimaterT.hh", "a00545.html", [
      [ "BaseDecimaterModule", "a00090.html", null ],
      [ "BaseDecimaterT", "a00091.html", "a00091" ]
    ] ],
    [ "CollapseInfoT.hh", "a00546.html", [
      [ "CollapseInfoT", "a00110.html", "a00110" ]
    ] ],
    [ "DecimaterT.cc", "a00547.html", "a00547" ],
    [ "DecimaterT.hh", "a00548.html", [
      [ "DecimaterT", "a00128.html", "a00128" ],
      [ "HeapInterface", "a00187.html", "a00187" ]
    ] ],
    [ "McDecimaterT.cc", "a00549.html", "a00549" ],
    [ "McDecimaterT.hh", "a00550.html", [
      [ "McDecimaterT", "a00203.html", "a00203" ]
    ] ],
    [ "MixedDecimaterT.cc", "a00551.html", "a00551" ],
    [ "MixedDecimaterT.hh", "a00552_source.html", null ],
    [ "ModAspectRatioT.cc", "a00553.html", "a00553" ],
    [ "ModAspectRatioT.hh", "a00554.html", [
      [ "ModAspectRatioT", "a00217.html", "a00217" ]
    ] ],
    [ "ModBaseT.hh", "a00555.html", "a00555" ],
    [ "ModEdgeLengthT.cc", "a00556.html", "a00556" ],
    [ "ModEdgeLengthT.hh", "a00557.html", [
      [ "ModEdgeLengthT", "a00220.html", "a00220" ]
    ] ],
    [ "ModHausdorffT.cc", "a00558.html", "a00558" ],
    [ "ModHausdorffT.hh", "a00559.html", [
      [ "ModHausdorffT", "a00222.html", "a00222" ]
    ] ],
    [ "ModIndependentSetsT.hh", "a00560_source.html", null ],
    [ "ModNormalDeviationT.hh", "a00561.html", [
      [ "ModNormalDeviationT", "a00225.html", "a00225" ]
    ] ],
    [ "ModNormalFlippingT.hh", "a00562.html", [
      [ "ModNormalFlippingT", "a00226.html", "a00226" ]
    ] ],
    [ "ModProgMeshT.cc", "a00563.html", "a00563" ],
    [ "ModProgMeshT.hh", "a00564.html", [
      [ "ModProgMeshT", "a00227.html", "a00227" ],
      [ "Info", "a00192.html", "a00192" ]
    ] ],
    [ "ModQuadricT.cc", "a00565.html", "a00565" ],
    [ "ModQuadricT.hh", "a00566.html", [
      [ "ModQuadricT", "a00228.html", "a00228" ]
    ] ],
    [ "ModRoundnessT.hh", "a00567.html", [
      [ "ModRoundnessT", "a00229.html", "a00229" ]
    ] ],
    [ "Observer.cc", "a00568.html", null ],
    [ "Observer.hh", "a00569.html", [
      [ "Observer", "a00237.html", "a00237" ]
    ] ]
];