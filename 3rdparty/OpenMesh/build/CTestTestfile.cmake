# CMake generated Testfile for 
# Source directory: /home/vasya/NetBeansProjects/SphereRecognition/3rdparty/OpenMesh
# Build directory: /home/vasya/NetBeansProjects/SphereRecognition/3rdparty/OpenMesh/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src/OpenMesh/Core)
subdirs(src/OpenMesh/Tools)
subdirs(src/OpenMesh/Apps)
subdirs(src/Unittests)
subdirs(Doc)
subdirs(src/Python)
