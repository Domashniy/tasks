Annotation:
This app reads a mesh file with .ply format and finds all spheres protruding from the flat field.
App prints parameters of the spheres i.e. positions of they centers and radii in file called res.txt.
Also it creates cloud.xyz file, which contains point cloud of initial surface added with points of refined spheres.

Build:
Run main_build.sh
It will build 3rdparty libs and executable file.
Or you could manually enter to the folder of OpenMesh and run make.
OpenMesh libs should be in foled "3rdparty/OpenMesh/install/libs" and includes 
should be in "3rdparty/OpenMesh/install/includes"

Run:
Just execute ./sphererecognition with a name of file which contains mesh.
No need to input number of spheres or any other parameters.


